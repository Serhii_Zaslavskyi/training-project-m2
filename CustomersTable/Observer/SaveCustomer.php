<?php
namespace Vaimo\CustomersTable\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

use Vaimo\CustomersTable\Model\GreaterValue;
use Vaimo\CustomersTable\Model\GreaterValueRepository;

class SaveCustomer implements ObserverInterface
{
    private $greaterValueFactory;
    private $greaterValueRepository;

    public function __construct(
        GreaterValue $greaterValueFactory,
        GreaterValueRepository $greaterValueRepository
    )
    {
        $this->greaterValueFactory = $greaterValueFactory;
        $this->greaterValueRepository = $greaterValueRepository;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getData('order');
        $customerId = $order->getCustomerEmail();
        $orderValue = $order->getTotalDue();

        if ($orderValue > 100) {
            $this->greaterValueFactory->setCustomerId($customerId);
            $this->greaterValueFactory->setOrderValue($orderValue);

            $this->greaterValueRepository->save($this->greaterValueFactory);
        }
    }
}
