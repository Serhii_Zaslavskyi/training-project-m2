<?php
namespace Vaimo\CustomersTable\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface GreaterValueInterface extends ExtensibleDataInterface
{
    const CUSTOMER_ID = 'customer_id';
    const ORDER_VALUE = 'value';

    public function getCustomerId();
    public function getOrderValue();
    public function setCustomerId($id);
    public function setOrderValue($value);
}
