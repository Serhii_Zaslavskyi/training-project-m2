<?php
namespace Vaimo\CustomersTable\Model\ResourceModel;


class GreaterValue extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('vaimo_customers', 'id');
    }

}
