<?php
namespace Vaimo\CustomersTable\Model\ResourceModel\GreaterValue;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'vaimo_customers';
    protected $_eventObject = 'customers_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vaimo\CheckoutNewStep\Model\GreaterValue', 'Vaimo\CheckoutNewStep\Model\ResourceModel\GreaterValue');
    }

}

