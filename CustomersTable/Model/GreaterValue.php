<?php
namespace Vaimo\CustomersTable\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;


class GreaterValue extends \Magento\Framework\Model\AbstractExtensibleModel implements
    \Vaimo\CustomersTable\Api\Data\GreaterValueInterface
{
    const CACHE_TAG = 'vaimo_customers';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Vaimo\CustomersTable\Model\ResourceModel\GreaterValue::class);
    }

    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    public function getOrderValue()
    {
        return $this->getData(self::ORDER_VALUE);
    }

    public function setCustomerId($id)
    {
        return $this->setData(self::CUSTOMER_ID, $id);
    }

    public function setOrderValue($value)
    {
        return $this->setData(self::ORDER_VALUE, $value);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
