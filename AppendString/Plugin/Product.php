<?php
namespace Vaimo\AppendString\Plugin;

use Magento\Catalog\Model\Product as CatalogProduct;

class Product
{
    public function afterGetName(CatalogProduct $subject, $result)
    {
        return $result . " test";
    }
}
