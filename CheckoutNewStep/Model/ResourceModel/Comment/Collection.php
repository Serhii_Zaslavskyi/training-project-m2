<?php
namespace Vaimo\CheckoutNewStep\Model\ResourceModel\Comment;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'comment_id';
    protected $_eventPrefix = 'vaimo_checkoutstep_comment';
    protected $_eventObject = 'comment_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Vaimo\CheckoutNewStep\Model\Comment', 'Vaimo\CheckoutNewStep\Model\ResourceModel\Comment');
    }

}

