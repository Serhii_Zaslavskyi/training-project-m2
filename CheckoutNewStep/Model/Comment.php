<?php
namespace Vaimo\CheckoutNewStep\Model;

/**
 * Class Comment
 */
class Comment extends \Magento\Framework\Model\AbstractModel implements
    \Vaimo\CheckoutNewStep\Api\Data\CommentInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'vaimo_checkoutstep_comment';

    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init(\Vaimo\CheckoutNewStep\Model\ResourceModel\Comment::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
