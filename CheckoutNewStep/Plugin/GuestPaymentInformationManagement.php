<?php
namespace Vaimo\CheckoutNewStep\Plugin;

use Magento\Quote\Api\Data\PaymentInterface;
use Vaimo\CheckoutNewStep\Model\CommentFactory;
use Vaimo\CheckoutNewStep\Model\CommentRepository;

/**
 * Class GuestPaymentInformationManagement
 */
class GuestPaymentInformationManagement
{
    /**
     * @var CommentFactory
     */
    protected $commentFactory;

    /**
     * @var CommentRepository
     */
    protected $commentRepository;

    /**
     * GuestPaymentInformationManagement constructor.
     * @param CommentFactory $commentFactory
     * @param CommentRepository $commentRepository
     */
    public function __construct(
        CommentFactory $commentFactory,
        CommentRepository $commentRepository
    ) {
        $this->commentFactory = $commentFactory;
        $this->commentRepository = $commentRepository;
    }

    public function afterSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Model\GuestPaymentInformationManagement $subject,
        $orderId,
        $cartId,
        $email,
        PaymentInterface $paymentMethod
    ) {
        $shippingComment = $paymentMethod->getExtensionAttributes();
        $comment = $shippingComment->getComment();

        if ($comment && $orderId) {
            $shippingCommentData = [
                'order_id' => $orderId,
                'comment' => $comment
            ];

            $shippingComment = $this->commentFactory->create();
            $shippingComment->setData($shippingCommentData);

            $this->commentRepository->save($shippingComment);
        }
    }
}
