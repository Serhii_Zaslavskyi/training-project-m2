<?php
namespace Vaimo\CheckoutNewStep\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface CommentInterface extends ExtensibleDataInterface
{
    public function getIdentities();
}
