<?php
namespace Vaimo\CheckoutNewStep\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Vaimo\CheckoutNewStep\Api\Data\CommentInterface;

interface CommentRepositoryInterface
{
    public function save(CommentInterface $object);
    public function getById($id);
    public function delete(CommentInterface $object);
    public function deleteById($id);
    public function getList(SearchCriteriaInterface $criteria);

}
